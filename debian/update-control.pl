#!/usr/bin/perl

use strict;
use warnings;
use Config::Model 2.085 qw/cme/;
use 5.10.0;

my $control = 'debian/control';

my $status = `git status --porcelain $control`;

die "$control is changed. Please commit or reset this file\n" if $status;

my $nqp_str = `nqp --version`;
chomp $nqp_str;
my ($nqp_v, $moar_v) 
    = ($nqp_str =~ /nqp version ([\d.]+) built on MoarVM version ([\d.]+)/);

die "Can't extract versions from '$nqp_str'\n"
    unless defined $nqp_v and defined $moar_v;

cme('dpkg-control')->modify(
    qq!source Build-Depends:=~"s/moarvm.*/moarvm-dev (>= $moar_v)/"! .
    qq!       Build-Depends:=~"s/nqp.*/nqp (>= $nqp_v)/"!
);

system(qw/git commit/, "-m" => "control: update moarvm-dev and nqp dep versions (cme)", $control);
