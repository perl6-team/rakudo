---
include: https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/salsa-ci.yml

variables:
  RAKU_DEPENDENT_REPOS: "https://salsa.debian.org/timotimo-guest/nqp https://salsa.debian.org/timotimo-guest/moarvm"
  RAKU_DEPENDENT_BRANCHES: 2024.09_attempt moar_2024.09_attempt
  RAKU_DEPENDENT_BRANCH_DEBUG: 0
  SALSA_CI_EXTRA_REPOSITORY: /etc/apt/sources.list.d/pkga.list
  RELEASE: experimental

.provisioning-find-aptly: &provisioning-find-aptly
  stage: provisioning
  image: $SALSA_CI_IMAGES_GBP
  dependencies: []
  extends:
    - .artifacts-default-expire
  script:
  - echo "looking for the aptly env :)"
  - '[[ $RAKU_DEPENDENT_BRANCH_DEBUG != 0 ]] && env'
  - git describe --always
  - apt-get update
  - apt-get install -y glab jq
  - |
    set -x
    echo "ref name is ${CI_COMMIT_REF_NAME} and RAKU_DEPENDENT_BRANCHES is ${RAKU_DEPENDENT_BRANCHES}"
    if [[ -z ${RAKU_DEPENDENT_BRANCHES} ]]; then
      echo "using commit ref name"
      export RAKU_DEPENDENT_BRANCHES=${GI_COMMIT_REF_NAME}
    else
      echo "using variable contents"
      export RAKU_DEPENDENT_BRANCHES=${RAKU_DEPENDENT_BRANCHES}
    fi
    echo "working on the repos in $RAKU_DEPENDENT_REPOS"
    DEP_JOB_ID=""
    for repo in $RAKU_DEPENDENT_REPOS; do
      echo "repo entry is $repo"
      glab ci list -R $repo -F json --status success -P 50 > glab_ci_list.json
      BRANCH_MATCHER=$(for bn in $RAKU_DEPENDENT_BRANCHES; do echo -n ".ref == \"$bn\" or "; done | sed -e 's/or $//')
      echo "constructed branch matcher is $BRANCH_MATCHER"
      DEP_PIPELINE_ID=$( cat glab_ci_list.json \
        | jq "map(select($BRANCH_MATCHER)) | first | .id" )
      echo "found pipeline ID $DEP_PIPELINE_ID"
      glab ci get -R $repo -b ${CI_COMMIT_REF_NAME} -F json -p ${DEP_PIPELINE_ID} > glab_ci_get.json
      echo "looking for the first pipeline job with name == aptly"
      DEP_JOB_ID="$DEP_JOB_ID "$( cat glab_ci_get.json \
        | jq '.jobs | map(select(.name == "aptly")) | first | .id')
    done
    echo "clean empty string from front of variable"
    DEP_JOB_ID=$(echo $DEP_JOB_ID | sed -e 's/^\s+//')
    echo "putting DEP_JOB_ID ($DEP_JOB_ID) into GITLAB_ENV file"
    echo "DEP_JOB_ID=\"$DEP_JOB_ID\"" >> $GITLAB_ENV
    echo "DEP_JOB_ID=\"$DEP_JOB_ID\"" >> aptly_dependency.env
  artifacts:
    reports:
      dotenv: aptly_dependency.env

before_script:
  - '[[ $RAKU_DEPENDENT_BRANCH_DEBUG != 0 ]] && printenv | sort'
  - |
    set -x
    if [[ -n "$DEP_JOB_ID" ]]; then
      echo "content of pkga.list before:"
      cat /etc/apt/sources.list.d/pkga.list || echo "doesn't exist"
      echo "" | tee /etc/apt/sources.list.d/pkga.list
      ls -lisah /etc/apt/sources.list.d/pkga.list
      echo "getting rid of those annoying dubquotes"
      DEP_JOB_ID=$(echo $DEP_JOB_ID | tr -d '"')
      echo "turning repos and ids into source list format"
      read -r -a repos <<< "$RAKU_DEPENDENT_REPOS"
      read -r -a jobids <<< "$DEP_JOB_ID"
      declare -p repos jobids
      amount=${#repos[@]}
      declare -p amount
      for ((i=0; i<amount; i++)); do
        echo "repo ${repos[i]} had job id ${jobids[i]}"
        echo "deb [trusted=yes] ${repos[i]}/-/jobs/${jobids[i]}/artifacts/raw/aptly ${RELEASE} main" | tee -a /etc/apt/sources.list.d/pkga.list
      done
      apt-get update
    fi

after_script:
  - '[[ $RAKU_DEPENDENT_BRANCH_DEBUG != 0 ]] && printenv | sort'
  - |
    set -x
    if [[ -n "$DEP_JOB_ID" ]]; then
      echo "forwarding DEP_JOB_ID to the next stage with salsa.env file"
      declare -p DEP_JOB_ID
      echo "DEP_JOB_ID=\"$DEP_JOB_ID\"" >> ${CI_PROJECT_DIR}/salsa.env
      echo "salsa.env file now contains:"
      cat ${CI_PROJECT_DIR}/salsa.env
    fi

.build-definition: &build-definition
  dependencies:
  - find-aptly
  - extract-source

.build-package: &build-package
  extends: 
  - .build-definition
  - .artifacts-default-expire

find-aptly:
  extends: .provisioning-find-aptly

extract-source:
  extends: .provisioning-extract-source

build:
  extends: .build-package

build i386:
  extends: .build-package-i386
build armel:
  extends: .build-package-armel

build armhf:
  extends: .build-package-armhf

build arm64:
  extends: .build-package-arm64

build riscv64:
  extends: .build-package-riscv64

build source:
  extends: .build-source-only

test-build-any:
  extends: .test-build-package-any

test-build-all:
  extends: .test-build-package-all

test-build-twice:
  extends: .test-build-package-twice

test-build-profiles:
  extends: .test-build-package-profiles

# test-crossbuild-arm64:
#  extends: .test-crossbuild-package-arm64
#    allow_failure: true

reprotest:
  extends: .test-reprotest
  allow_failure: true

lintian:
  extends: .test-lintian

autopkgtest:
  extends: .test-autopkgtest

blhc:
  extends: .test-blhc

piuparts:
  extends: .test-piuparts

missing-breaks:
  extends: .test-missing-breaks

rc-bugs:
  extends: .test-rc-bugs

wrap-and-sort:
  extends: .test-wrap-and-sort

aptly:
  extends: .publish-aptly
