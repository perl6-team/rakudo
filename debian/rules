#!/usr/bin/make -f

PACKAGE = $(shell dh_listpackages)
INSTALL     = $(CURDIR)/debian/rakudo

# if the Debian package version is needed:
NQP_VERSION = $(shell dpkg -s nqp | sed -rne 's,^Version: (.+).*,\1,p')
#MOARVM_VERSION = $(shell dpkg -s moarvm | sed -rne 's,^Version: (.+).*,\1,p')

# get only upstream version
#NQP_VERSION    = $(shell nqp --version  | perl -n -E 'm/([\d.]+)/; say $$1;')
MOARVM_VERSION = $(shell moar --version | perl -n -E 'm/([\d.]+)/; say $$1;')

NQP_NEXT    = $(shell nqp --version  | perl -n -E 'my ($$y,$$m) = m/(2\d+)\.(\d+)/; if ($$m < 12) { $$m++} else {$$m=1;$$y++}; printf("%d.%02d\n",$$y,$$m);')
MOARVM_NEXT = $(shell moar --version | perl -n -E 'my ($$y,$$m) = m/(2\d+)\.(\d+)/; if ($$m < 12) { $$m++} else {$$m=1;$$y++}; printf("%d.%02d\n",$$y,$$m);')

# TODO: read the file without cat ?
RAKU_VERSION = $(shell cat VERSION)

# By default, perl t/harness5 runs 6 tests in parallel. This is fine
# on powerful system but tends to mess up tests on mips or armhf.
export TEST_JOBS = 1

# enable verbose tests
export HARNESS_VERBOSE = 1

# performance tests are not reliable over architectures
export RAKUDO_SKIP_TIMING_TESTS = 1

# prevent test failure at t/05-messages/03-errors.t line 92
export HOME=/tmp/

%:
	dh $@

override_dh_auto_configure:
	perl -p -i -e 's/%%Debian-compiler-id%%/Debian-$(RAKU_VERSION)/' src/core.c/Compiler.pm6
	perl Configure.pl --prefix=/usr --perl6-home=/usr/lib/perl6 --backends=moar

# rakudo needs a strict dependency version of nqp. See README.source
# for details.
# WARNING: rakudo must be compiled with the exact nqp that is shipped to
# Debian. Otherwise timestamp will differ and rakudo will break on user
# systems.
# WARNING: any modification of the source leads to a new compiler id, which obsoletes
# existing pre-compiled files. That's why ApiVersion now contains the 8 first char of
# the compiler id.
override_dh_gencontrol:
	dh_gencontrol -- -Vnqp:Depends="nqp (>= $(NQP_VERSION)), nqp (<< $(NQP_NEXT))" \
	                 -Vmoarvm:Depends="moarvm (>= $(MOARVM_VERSION)), moarvm(<< $(MOARVM_NEXT))" \
					 -Vraku:ApiVersion="raku-api-$(RAKU_VERSION)+"$$(./rakudo-m -e 'say substr($$*PERL.compiler.id.lc,0,8)')

override_dh_auto_build:
	dh_auto_build
	echo "Configuration:"
	echo -n "NQP source-digest: "
	nqp --show-config | grep source-digest
	echo -n "raku source-digest: "
	./rakudo-m --show-config | grep source-digest
	echo -n "raku compiler id: "
	./rakudo-m -e 'say $$*PERL.compiler.id;'
	pod2man --name=perl6 docs/running.pod debian/perl6.1

override_dh_auto_install:
	dh_auto_install
	find $(INSTALL) -type d -empty -delete
	mkdir -p $(INSTALL)/usr/share/perl6/
	./rakudo-m -e 'say $$*PERL.compiler.id' > $(INSTALL)/usr/share/perl6/rakudo-compiler-id
	install --mode=a+x debian/rakudo-helper.pl $(INSTALL)/usr/share/perl6/rakudo-helper.pl
	mkdir -p $(INSTALL)/var/lib/perl6/modules
	mkdir -p $(INSTALL)/usr/share/perl6/debian-sources
	mkdir -p $(INSTALL)/usr/lib/perl6/site/short
	mkdir -p $(INSTALL)/usr/lib/perl6/vendor/short

# we currently (2018.12) want to run the tests multiple times on failures, to
# get more data on flappers
override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
# see https://github.com/rakudo/rakudo/issues/2567#issuecomment-572489015
	-$(RM) t/08-performance/99-misc.t
	echo "Skipping broken test. See https://github.com/rakudo/rakudo/issues/3824"
	-$(RM) t/09-moar/01-profilers.t
# temporarily skip a flaky test https://github.com/rakudo/rakudo/issues/4726
	-$(RM) t/02-rakudo/repl.t
	make test || make test || make test || make test || MVM_SPESH_DISABLE=1 make test
endif

override_dh_missing:
	dh_missing --list-missing
